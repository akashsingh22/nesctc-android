package com.nesctc.guardconnect.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;
import com.nesctc.guardconnect.R;
import com.nesctc.guardconnect.gpsdetctor.SimpleLocation;
import com.nesctc.guardconnect.pref.PrefManager;
import com.nesctc.guardconnect.reciver.ConnectivityReceiver;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import me.dm7.barcodescanner.core.CameraPreview;
import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class BarCodeScanningActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    ZXingScannerView mScannerView;
    private static int SPLASH_SHOW_TIME = 2500;
    private boolean barcodeScanned = false;
    private boolean previewing = true;
    private android.hardware.Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler;
    private String scanResult, rawScanResult;
    private TextView scoreview;
     private Context mcontext;
     public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static final String TAG = "BarCodeScanningActivity";
    private PrefManager prefManager;
    private ProgressDialog progressDialog;
    private String url="";
    private SimpleLocation location;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_main);
        scoreview = (TextView) findViewById(R.id.score);
         mcontext = BarCodeScanningActivity.this;
          prefManager = PrefManager.getInstance(mcontext);
        location = new SimpleLocation(this);
        progressDialog = new ProgressDialog(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndRequestPermissions();
        }

        if (ConnectivityReceiver.isConnected(mcontext)) {
            scoreview.setVisibility(View.GONE);
        } else {
            scoreview.setVisibility(View.VISIBLE);

        }

        mScannerView = new ZXingScannerView(BarCodeScanningActivity.this);   // Programmatically initialize the scanner view
        RelativeLayout view = (RelativeLayout) findViewById(R.id.view);
        view.addView(mScannerView); //  setContentView(mScannerView);

    }

    @Override
    public void onPause() {
        super.onPause();
        location.endUpdates();
        mScannerView.stopCamera();          // Stop camera on pause
    }



    @Override
    public void handleResult(Result rawResult) {

        rawScanResult = rawResult.getText().toString();
        if(ConnectivityReceiver.isConnected(this)){
            new UploadScanBarCodeTask().execute();
        } else {
            Toast.makeText(this, "Internet is not available !", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * Async Task for Upload Scan BarCode
     *
     * @author akash
     */
    private class UploadScanBarCodeTask extends AsyncTask<String, String, String> {

        private String sb = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!progressDialog.isShowing()) {
                progressDialog.setMessage("Please wait...");
                progressDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String responseResult = "";
           String url ="http://104.236.40.68/guardconnect/scanbarcode.php?&phone="+prefManager.getString("phone")+"&IVRJobID="+prefManager.getString("ivrId")+"&data="+rawScanResult+
                   "&lat="+location.getLatitude()+"&lon="+location.getLongitude();

            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpGet = new HttpPost(url);
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                String xml = EntityUtils.toString(httpEntity);
                Log.d("Barcode Response: ", xml);
                responseResult = xml;

            } catch (Exception e) {
                Log.d("Barcode exception: ", e.getMessage());
                e.printStackTrace();
            }

            return responseResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("Barcode response",result);
            if(progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }

    }


    private void methodBlank() {
        mScannerView.resumeCameraPreview(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mScannerView.setResultHandler(BarCodeScanningActivity.this);
        location.beginUpdates();
        mScannerView.startCamera();
        methodBlank();
    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(BarCodeScanningActivity.this,
                Manifest.permission.CAMERA);
        int locationPermission = ContextCompat.checkSelfPermission(BarCodeScanningActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        int write = ContextCompat.checkSelfPermission(BarCodeScanningActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readinternal = ContextCompat.checkSelfPermission(BarCodeScanningActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (readinternal != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (write != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(BarCodeScanningActivity.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);

                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "sms & location services permission granted");
                    } else {
                        Log.d(TAG, "some permissions are not granted ask again ");
                        if (ActivityCompat.shouldShowRequestPermissionRationale(BarCodeScanningActivity.this, Manifest.permission.SEND_SMS) ||
                                ActivityCompat.shouldShowRequestPermissionRationale(BarCodeScanningActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                            showDialogOK("SMS and Location Services Permission are required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    break;
                                            }
                                        }
                                    });
                        } else {
                            // Toast.makeText(BarCodeScanningActivity.this, "go to settings and enable permissions", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        }
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(BarCodeScanningActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

}