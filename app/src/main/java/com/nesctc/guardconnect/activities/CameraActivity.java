package com.nesctc.guardconnect.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.nesctc.guardconnect.R;
import com.nesctc.guardconnect.pref.PrefManager;
import com.nesctc.guardconnect.reciver.ConnectivityReceiver;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;




public class CameraActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imageView;
    private Button btnTakeAgain,btnSend;
    private static final int CAMERA_REQUEST = 1;
    private boolean firstTime = true;
    private EditText etDescription;
    private PrefManager prefManager;
    private String strDescription="";
    private ProgressDialog progressDialog;
    private String imageBase64 = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        firstTime = true;
        progressDialog = new ProgressDialog(this);
        prefManager =PrefManager.getInstance(this);
        imageView =(ImageView)findViewById(R.id.image);
        btnTakeAgain =(Button)findViewById(R.id.btn_take_again);
        btnSend =(Button)findViewById(R.id.btn_send);
        etDescription =(EditText)findViewById(R.id.et_description);
        btnTakeAgain.setOnClickListener(this);
        btnSend.setOnClickListener(this);
        openCamera();


    }

    private void openCamera() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_take_again:
                openCamera();
                break;
            case R.id.btn_send:
              if(!TextUtils.isEmpty(etDescription.getText().toString().trim())){
                  strDescription =etDescription.getText().toString().trim();
                  if(ConnectivityReceiver.isConnected(CameraActivity.this)){
                      new UploadImageTask().execute();
                  } else {
                      Toast.makeText(CameraActivity.this, "Internet is not available !", Toast.LENGTH_SHORT).show();
                  }

              }else{
                  Toast.makeText(CameraActivity.this,"Please write Description",Toast.LENGTH_SHORT).show();
              }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(photo);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            if (photo != null) {
                imageBase64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
            } else {
                imageBase64 = "";
            }
            firstTime = false;
        }else {
            if(firstTime) {
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        }
    }

    /**
     * Async Task for Upload Image Task
     *
     * @author akash
     */
    private class UploadImageTask extends AsyncTask<String, String, String> {

        private String sb = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!progressDialog.isShowing()) {
                progressDialog.setMessage("Please wait...");
                progressDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String responseResult = "";

           // String url ="http://104.236.40.68/guardconnect/photo.php?Description="+strDescription+"&PhoneNumber="+prefManager.getString("phone")+"&IVRJobID="+prefManager.getString("ivrId")+"&PhotoTaken="+imageBase64;

            String url ="http://104.236.40.68/guardconnect/sg_connect/photo.php?Description="+strDescription+"&phone="+prefManager.getString("phone")+"&IVRJobID="+prefManager.getString("ivrId")+"&PhotoTaken="+imageBase64;

            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpGet = new HttpPost(url);
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                String xml = EntityUtils.toString(httpEntity);
                Log.d("photo Response: ", xml);
                responseResult = xml;

            } catch (Exception e) {
                Log.d("photo exception: ", e.getMessage());
                e.printStackTrace();
            }

            return responseResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("response",result);
            if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }

    }

}
