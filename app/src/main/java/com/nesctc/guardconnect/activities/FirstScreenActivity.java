package com.nesctc.guardconnect.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nesctc.guardconnect.R;
import com.nesctc.guardconnect.gpsdetctor.GpsStatusDetector;
import com.nesctc.guardconnect.gpsdetctor.SimpleLocation;
import com.nesctc.guardconnect.model.FirstModel;
import com.nesctc.guardconnect.pref.PrefManager;
import com.nesctc.guardconnect.reciver.ConnectivityReceiver;
import com.nesctc.guardconnect.service.LocationUploadService;
import com.nesctc.guardconnect.utils.HandleXML;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class FirstScreenActivity  extends AppCompatActivity implements GpsStatusDetector.GpsStatusDetectorCallBack{

    private Button btnCheckIn,btnCall;
    private GpsStatusDetector mGpsStatusDetector;
    private SimpleLocation location;
    private Double latitude, longitude;
    private Context context;
    private EditText etGuardDName,etGuardPhone,etGuardId,etManager;
    private ProgressDialog progressDialog;
    private PrefManager prefManager;
    private String firstUrl="",secondUrl="",checkInUrl="";
    private String guardName,guardPhone,ivrId;
    private String strTime = "";
    private String guardId="";
    public  final int PERMISSION_CODE1 = 42043;
    public  final int PERMISSION_CODE = 42042;
    private List<FirstModel> firstModels = new ArrayList<>();
    private LinearLayout llOnline,llOffline;
    private Button btnOffCall;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_screen);
        context = this;
        progressDialog =new ProgressDialog(context);
        prefManager = PrefManager.getInstance(context);
        mGpsStatusDetector = new GpsStatusDetector(this);
        location = new SimpleLocation(this);

        llOnline =(LinearLayout)findViewById(R.id.ll_online);
        llOffline =(LinearLayout)findViewById(R.id.ll_offline);
        btnOffCall =(Button)findViewById(R.id.btn_offcall);
        btnCheckIn =(Button)findViewById(R.id.btn_check_in);
        btnCall =(Button)findViewById(R.id.btn_call);
        etGuardDName =(EditText)findViewById(R.id.et_guard_name);
        etGuardPhone =(EditText)findViewById(R.id.et_phone);
        etGuardId =(EditText)findViewById(R.id.et_job_id);
        etManager =(EditText)findViewById(R.id.et_manager);

        etGuardId.setText(prefManager.getString("ivrId"));
        etGuardDName.setText(prefManager.getString("name"));
        etGuardPhone.setText(prefManager.getString("phone"));
        etGuardDName.setSelection(etGuardDName.getText().length());
        initListener();
//        llOnline.setVisibility(View.VISIBLE);
//        mGpsStatusDetector.checkGpsStatus();
//        popupforGrantLocation();
        if (ConnectivityReceiver.isConnected(context)) {
            new CheckGuardTask().execute();
        }else{
            Toast.makeText(context, "Internet is not available !", Toast.LENGTH_SHORT).show();
            llOffline.setVisibility(View.VISIBLE);
        }

//        Guard Name: Mike
//        Phone 4014051349
//        IvrID 8100178

    }

    private void initListener() {
        btnOffCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!checkCallPermission()){
                    ActivityCompat.requestPermissions(FirstScreenActivity.this,
                            new String[]{android.Manifest.permission.CALL_PHONE},
                            PERMISSION_CODE1);

                }else{
                    callSupport();
                }
            }
        });

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!checkCallPermission()){
                    ActivityCompat.requestPermissions(FirstScreenActivity.this,
                            new String[]{android.Manifest.permission.CALL_PHONE},
                            PERMISSION_CODE1);

                }else{
                    callSupport();
                }
            }
        });

        btnCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(location.hasLocationEnabled()) {
                    if (!checkFineLocationPermission() || !checkCoarseLocationPermission()) {
                        ActivityCompat.requestPermissions(FirstScreenActivity.this,
                                new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION
                                }, PERMISSION_CODE);
                    }else {
                        boolean check = checkValue();
                        if (check) {
                            if (ConnectivityReceiver.isConnected(context)) {
                                prefManager.putString("ivrId", etGuardId.getText().toString().trim());
                                prefManager.putString("name", etGuardDName.getText().toString().trim());
                                prefManager.putString("phone", etGuardPhone.getText().toString().trim());
                                prefManager.putString("guardId",  etGuardPhone.getText().toString().trim().substring( etGuardPhone.getText().toString().trim().length() - 4));
                                //prefManager.putString("manager", etManager.getText().toString().trim());
                                guardName =etGuardDName.getText().toString().trim();
                                guardPhone= etGuardPhone.getText().toString().trim();
                                ivrId = etGuardId.getText().toString().trim();
                                guardId = etGuardPhone.getText().toString().trim().substring( etGuardPhone.getText().toString().trim().length() - 4);
                                Calendar now = Calendar.getInstance();
                                SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyyhh:mm:ssa");
                                strTime = df.format(now.getTime());

                                new FirstValidationTask().execute();

                            } else {
                                Toast.makeText(context, "Internet is not available !", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }else{
                    mGpsStatusDetector.checkGpsStatus();
                }

            }
        });
    }

    private void showDialogForLocation(final String site) {
// custom dialog

        final Dialog dialog = new Dialog(context,R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirmation);

        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_detail);
        text.setText("Would you like check into "+site);
        TextView tvYes = (TextView) dialog.findViewById(R.id.tv_yes);
        TextView tvNo = (TextView) dialog.findViewById(R.id.tv_no);
        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showCheckInDialog(site);
            }
        });
        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showCheckInDialog(final String site) {

        // custom dialog
        final Dialog dialog = new Dialog(context,R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_checkin_place);

        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_detail);
        text.setText("Checked in at Job Site: "+site);
        TextView tvOk = (TextView) dialog.findViewById(R.id.tv_ok);
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                Intent intent = new Intent(FirstScreenActivity.this, MainActivity.class);
                intent.putExtra("address", location.getAddressLine(context));
                intent.putExtra("locality", location.getLocality(context));
                intent.putExtra("country", location.getCountryName(context));
                intent.putExtra("latitude", latitude == 0.0?location.getLatitude():latitude);
                intent.putExtra("longitude", longitude== 0.0?location.getLongitude():longitude);
                startActivity(intent);
                startService(new Intent(FirstScreenActivity.this, LocationUploadService.class));

            }
        });
        dialog.show();
    }


    private void showErrorDialog(final String msg) {
// custom dialog

        final Dialog dialog = new Dialog(context,R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirmation);

        // set the custom dialog components - text, image and button
        TextView textHeader = (TextView) dialog.findViewById(R.id.tv_header);
        textHeader.setVisibility(View.GONE);
        TextView text = (TextView) dialog.findViewById(R.id.tv_detail);
        text.setText(msg);
        TextView tvYes = (TextView) dialog.findViewById(R.id.tv_yes);
        tvYes.setText("Call");
        TextView tvNo = (TextView) dialog.findViewById(R.id.tv_no);
        tvNo.setText("Cancel");
        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(!checkCallPermission()){
                    ActivityCompat.requestPermissions(FirstScreenActivity.this,
                            new String[]{android.Manifest.permission.CALL_PHONE},
                            PERMISSION_CODE1);

                }else{
                    callSupport();
                }
            }
        });
        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private boolean checkValue() {
        if(TextUtils.isEmpty(etGuardDName.getText().toString().trim())){
            Toast.makeText(context,"Please fill guard name",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(TextUtils.isEmpty(etGuardPhone.getText().toString().trim())){
            Toast.makeText(context,"Please fill guard phone number",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(etGuardPhone.getText().toString().trim().length()<10){
            Toast.makeText(context,"Please fill valid guard phone number",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(TextUtils.isEmpty(etGuardId.getText().toString().trim())){
            Toast.makeText(context,"Please fill guard IVR Id",Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    @Override
    public void onGpsSettingStatus(boolean enabled) {
        Log.d("Gps enabled: ", "" + enabled);
        //showSnackBar(enabled ? "GPS enabled" : "GPS disabled");
        if (enabled) {

        }
    }

    @Override
    public void onGpsAlertCanceledByUser() {
        // showSnackBar("canceledByUser");
    }


    // Showing SnackBar
    private void showSnackBar(String text) {
        Toast.makeText(FirstScreenActivity.this, text, Toast.LENGTH_SHORT).show();
    }



    public boolean checkFineLocationPermission() {
        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public boolean checkCoarseLocationPermission() {
        String permission = "android.permission.ACCESS_COARSE_LOCATION";
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
    public boolean checkCallPermission() {
        String permission = "android.permission.CALL_PHONE";
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mGpsStatusDetector.checkOnActivityResult(requestCode, resultCode);
    }

    @Override
    public void onResume() {
        super.onResume();
        // make the device update its location
        location.beginUpdates();
        if(llOnline.getVisibility() == View.VISIBLE)
        popupforGrantLocation();


    }

    private void popupforGrantLocation() {

        if (!checkFineLocationPermission() || !checkCoarseLocationPermission()) {
            ActivityCompat.requestPermissions(FirstScreenActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION
                    }, 1);
        }
        latitude = location.getLatitude();
        longitude = location.getLongitude();

        location.setListener(new SimpleLocation.Listener() {
            @Override
            public void onPositionChanged() {
                latitude = location.getLatitude();
                longitude = location.getLongitude();

            }
        });
    }

    @Override
    protected void onPause() {
        // stop location updates (saves battery)
        location.endUpdates();
        super.onPause();
    }

    /**
     * Async Task for Check Guard
     *
     * @author akash
     */
    private class CheckGuardTask extends AsyncTask<String, String, String> {

        private String sb = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!progressDialog.isShowing()) {
                progressDialog.setMessage("Please wait...");
                progressDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String responseResult = "";
            String Url ="http://104.236.40.68/guardconnect/sg_connect/status.php";

            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(Url);

                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                String xml = EntityUtils.toString(httpEntity);
                Log.d("Response: ", xml);
                HandleXML handleXML = new HandleXML();
                Document doc = handleXML.getDomElement(xml);
                NodeList variable = doc.getElementsByTagName("guardconnect");
                Node aNode=variable.item(0);
               responseResult = aNode.getFirstChild().getNodeValue();


            } catch (SocketTimeoutException e) {

            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

           if(result.equalsIgnoreCase("online")){
               llOnline.setVisibility(View.VISIBLE);
               mGpsStatusDetector.checkGpsStatus();
               popupforGrantLocation();
           }else if(result.equalsIgnoreCase("offline")){
               llOffline.setVisibility(View.VISIBLE);
           }


        }

    }

    /**
     * Async Task for First Validation
     *
     * @author akash
     */
    private class FirstValidationTask extends AsyncTask<String, String, List<FirstModel>> {

        private String sb = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!progressDialog.isShowing()) {
                progressDialog.setMessage("Please wait...");
                progressDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }

        @Override
        protected List<FirstModel> doInBackground(String... params) {
            String responseResult = "";
            firstModels.clear();
            firstUrl ="http://70.166.150.196/safeguard/sgSitetracker?Command=Validate&PhoneNumber="+guardPhone+"&GuardID="+guardId +"&Manager=&CurrentTime="+strTime+"&ErrorNextPage=9990&GuardName="+guardName+"&InvalidNextPage=350&JobIDConf=false&JobIDRetry=0&ValidNextPage=375&ivrJobID="+ivrId+"&CanCheckIn=1";

             try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(firstUrl);

                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                String xml = EntityUtils.toString(httpEntity);
                Log.d("Response: ", xml);
                HandleXML handleXML = new HandleXML();
                Document doc = handleXML.getDomElement(xml);
                NodeList variable = doc.getElementsByTagName("VARIABLES");
                for(int i =0;i<variable.getLength();i++){

                    Node aNode=variable.item(i);
                    Element fstElmnt = (Element) aNode;

                    NodeList websiteList = fstElmnt.getElementsByTagName("VAR");
                    int check=websiteList.getLength();

                    for(int k=0;k<check;k++)
                    {
                        FirstModel  firstModel = new FirstModel();
                        Node checkNode=websiteList.item(k);
                        Element websiteElement = (Element) checkNode;
                        firstModel.setName(websiteElement.getAttribute("name"));
                        firstModel.setValue(websiteElement.getAttribute("value"));
                        firstModels.add(firstModel);
                    }

                }

            } catch (SocketTimeoutException e) {

            } catch (Exception e) {
                e.printStackTrace();
            }

            return firstModels;
        }

        @Override
        protected void onPostExecute(List<FirstModel> result) {
            super.onPostExecute(result);

            for (int i =0;i<result.size();i++){
                if(result.get(i).getName().equalsIgnoreCase("ValidationMessage") && result.get(i).getValue().equalsIgnoreCase("")){
                    prefManager.putString("site",result.get(2).getValue());
                    new SecondValidationTask().execute();
                }else  if(result.get(i).getName().equalsIgnoreCase("ValidationMessage")){
                    progressDialog.dismiss();
                    Toast.makeText(context,result.get(i).getValue(),Toast.LENGTH_SHORT).show();
                }

            }

        }

    }


    /**
     * Async Task for Second Validation
     *
     * @author akash
     */
    private class SecondValidationTask extends AsyncTask<String, String, String> {

        private String sb = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            String responseResult = "";

            secondUrl =  "http://104.236.40.68/guardconnect/clockin.php?Command=Validate&CurrentTime="+strTime+"&ErrorNextPage=9990&PhoneNumber="+guardPhone+"&GuardID="+guardId+"&Manager=&GuardName="+guardName+"&InvalidNextPage=350&JobIDConf=false&JobIDRetry=0&ValidNextPage=375&ivrJobID="+ivrId+"&CanCheckIn=1&lat="+latitude+"&lon="+longitude;

            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(secondUrl);

                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                String response = EntityUtils.toString(httpEntity);
                Log.d("Response: ", response);
               responseResult =response;
            } catch (SocketTimeoutException e) {

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if(result.equalsIgnoreCase("Successful")){
                showDialogForLocation(prefManager.getString("site"));
            }else{
                showErrorDialog("There is an issue with IVR job ID you have entered. Please try again or call operator at 401-921-1002");

            }
        }

    }


    /**
     * Async Task for CheckInCommand
     *
     * @author akash
     */
    private class CheckInValidationTask extends AsyncTask<String, String, List<FirstModel>> {

        private String sb = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected List<FirstModel> doInBackground(String... params) {
            String responseResult = "";
            firstModels.clear();
           //checkInUrl = "http://70.166.150.196/safeguard/sgSitetracker?GuardID="+guardId+"&JobID=&ValidNextPage=1600&Command=CheckIn&CIOTime="+strTime+"&ivrJobID="+ivrId+"&JobInfoCI=BAL003-0017%3BBAL0003052717%3B1&CIDate="+strTime+"&ErrorNextPage=9999&PhoneNumber="+guardPhone+"&subscriberID=400000&InvalidNextPage=1700&lat="+latitude+"&lon="+longitude;
            checkInUrl = "http://104.236.40.68/guardconnect/sg_connect/sitetracker.php?GuardID="+guardId+"&JobID=&ValidNextPage=1600&Command=CheckIn&CIOTime="+strTime+"&ivrJobID="+ivrId+"&JobInfoCI=BAL003-0017%3BBAL0003052717%3B1&CIDate="+strTime+"&ErrorNextPage=9999&PhoneNumber="+guardPhone+"&subscriberID=400000&InvalidNextPage=1700&lat="+latitude+"&lon="+longitude;

            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(checkInUrl);

                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                String xml = EntityUtils.toString(httpEntity);
                Log.d("CheckIn Response: ", xml);
                HandleXML handleXML = new HandleXML();
                Document doc = handleXML.getDomElement(xml);
                NodeList variable = doc.getElementsByTagName("VARIABLES");
                for(int i =0;i<variable.getLength();i++){

                    Node aNode=variable.item(i);
                    Element fstElmnt = (Element) aNode;

                    NodeList websiteList = fstElmnt.getElementsByTagName("VAR");
                    int check=websiteList.getLength();

                    for(int k=0;k<check;k++)
                    {
                        FirstModel  firstModel = new FirstModel();
                        Node checkNode=websiteList.item(k);
                        Element websiteElement = (Element) checkNode;
                        firstModel.setName(websiteElement.getAttribute("name"));
                        firstModel.setValue(websiteElement.getAttribute("value"));
                        firstModels.add(firstModel);
                    }

                }

            } catch (SocketTimeoutException e) {

            } catch (Exception e) {
                e.printStackTrace();
            }

            return firstModels;
        }

        @Override
        protected void onPostExecute(List<FirstModel> result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            showDialogForLocation(prefManager.getString("site"));
//            for (int i =0;i<result.size();i++){
//                if(result.get(i).getName().equalsIgnoreCase("ValidationMessage") && result.get(i).getValue().equalsIgnoreCase("")){
//
//                      showDialogForLocation(prefManager.getString("site"));
//                }else  if(result.get(i).getName().equalsIgnoreCase("ValidationMessage")){
//
//                    Toast.makeText(context,result.get(i).getValue(),Toast.LENGTH_SHORT).show();
//                }
//
//            }

        }

    }

    private void  callSupport(){

        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:4019211002"));
        startActivity(callIntent);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // construct a new instance of SimpleLocation
                    location = new SimpleLocation(this);
                    if (!location.hasLocationEnabled()) {
                        // ask the user to enable location access
                        mGpsStatusDetector.checkGpsStatus();

                    } else  {
                        location.setListener(new SimpleLocation.Listener() {
                            @Override
                            public void onPositionChanged() {
                                if (location.getLatitude() != 0.0) {
                                    latitude = location.getLatitude();
                                }
                            }
                        });

                    }

                }
             break;

            case PERMISSION_CODE1:
                callSupport();
                break;

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
