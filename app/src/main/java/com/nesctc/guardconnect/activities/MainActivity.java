package com.nesctc.guardconnect.activities;


import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.nesctc.guardconnect.pref.PrefManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.nesctc.guardconnect.R;
import com.nesctc.guardconnect.reciver.ConnectivityReceiver;
import com.nesctc.guardconnect.service.LocationUploadService;
import com.nesctc.guardconnect.views.CustomScrollView;


public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    private GoogleMap mMap;
    private Double latitude, longitude;
    private String locationName = "";
    private Button btnCheckout, btnTimeSheet, btnTakePhoto,btnScanBarCode;
    private TextView tvId, tvAddress, tvTime;
    private PrefManager prefManager;
    private String address,locality,country;
    private Context context;
    private String time,date,apitime;
    private String imageBase64CheckIn="",imageBase64CheckOut="";
    private String strCheckOutManagerName="",strCheckInManagerName="";
    private ProgressDialog progressDialog;
    private String strRating="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context =this;
        prefManager = PrefManager.getInstance(this);
        progressDialog = new ProgressDialog(this);
        final Intent intent = getIntent();
        locationName = intent.getStringExtra("locationName");
        address = intent.getStringExtra("address");
        locality =intent.getStringExtra("locality");
        country = intent.getStringExtra("country");

        latitude = intent.getDoubleExtra("latitude", 0.0);
        longitude = intent.getDoubleExtra("longitude", 0.0);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        tvId = (TextView) findViewById(R.id.tv_ivr_job_id);
        tvAddress = (TextView) findViewById(R.id.tv_address);
        tvTime = (TextView) findViewById(R.id.tv_clock);
        Calendar now = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat df1 = new SimpleDateFormat("MM-dd-yyyy");
        SimpleDateFormat df2 = new SimpleDateFormat("hh:mma");
        time =df.format(now.getTime());
        date =df1.format(now.getTime());
        apitime =df2.format(now.getTime());

        tvId.setText("IVR Job ID: "+prefManager.getString("ivrId"));
        tvAddress.setText(prefManager.getString("site"));
        tvTime.setText("Clock In: "+time+", "+date);

        btnCheckout = (Button) findViewById(R.id.btn_check_out);
        btnTimeSheet = (Button) findViewById(R.id.btn_time_sheet);
        btnTakePhoto = (Button) findViewById(R.id.btn_take_photo);
        btnScanBarCode =(Button) findViewById(R.id.btn_barcode);

        btnCheckout.setOnClickListener(this);
        btnTimeSheet.setOnClickListener(this);
        btnTakePhoto.setOnClickListener(this);
        btnScanBarCode.setOnClickListener(this);

        // Dialog Popup
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                showCheckInDialog("Please find the manager. Once you have located the manager please click timesheet and have your manager enter their name and sign to validate your arrival time. If there is no manger present please call operations and inform them.");
            }
        }, 1000);


    }

    private void showCheckInDialog(final String site) {

        // custom dialog
        final Dialog dialog = new Dialog(context,R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_checkin_place);

        // set the custom dialog components - text, image and button
        TextView  tvTitle = (TextView) dialog.findViewById(R.id. tvTitle);
        tvTitle.setText("NESCTC GuardConnect");
        TextView text = (TextView) dialog.findViewById(R.id.tv_detail);
        text.setText(site);
        TextView tvOk = (TextView) dialog.findViewById(R.id.tv_ok);
       tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (latitude != null && longitude != null) {
            LatLng sydney = new LatLng(latitude, longitude);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 12));
            //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(itemLat, itemLong), 7));
        }
    }


    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.btn_check_out:
                if(imageBase64CheckIn.equalsIgnoreCase("")){
                  Toast.makeText(MainActivity.this,"Please Complete CheckIn",Toast.LENGTH_SHORT).show();
                }else{
                  if(ConnectivityReceiver.isConnected(MainActivity.this)) {
                      new CheckOutTask().execute();
                  }
                }


                break;
            case R.id.btn_take_photo:
                if (!checkReadExternalPermission() || !checkWriteExternalPermission() || !checkCameraPermission()) {
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA,
                            }, 1);
                } else {

                  openCameraActivity();
                }

                break;
            case R.id.btn_time_sheet:
                if (!checkReadExternalPermission() || !checkWriteExternalPermission() ) {
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE
                            }, 2);
                } else {

                    showCheckInDialog();
                }

                break;

            case R.id.btn_barcode:
                openBarCodeActivity();
                break;
        }
    }

    private void showPopup() {
        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_checkin_place);

        // set the custom dialog components - text, image and button
        TextView tvTitle= (TextView)dialog.findViewById(R.id.tvTitle);
        tvTitle.setText("Checking out");
        TextView text = (TextView) dialog.findViewById(R.id.tv_detail);
        text.setText("Checking out of Job Site: "+prefManager.getString("site")+"\nManager must be present for signature!");
        TextView tvOk = (TextView) dialog.findViewById(R.id.tv_ok);
        tvOk.setText("Manager Present");
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showCheckOutDialog();
            }
        });
        dialog.show();

    }

    private void openCameraActivity() {
        Intent intent =new Intent(MainActivity.this,CameraActivity.class);
        startActivity(intent);
    }

    private void openBarCodeActivity() {
        Intent intent =new Intent(MainActivity.this,BarCodeScanningActivity.class);
        startActivity(intent);
    }

    public boolean checkWriteExternalPermission() {
        String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public boolean checkReadExternalPermission() {
        String permission = "android.permission.READ_EXTERNAL_STORAGE";
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public boolean checkCameraPermission() {
        String permission = "android.permission.CAMERA";
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private void showCheckOutDialog() {
        // custom dialog
        final Dialog dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_checkout);
        final EditText etManagerName =(EditText)dialog.findViewById(R.id.et_manager_name);
        if(!TextUtils.isEmpty(prefManager.getString("manager"))){
            etManagerName.setText(prefManager.getString("manager"));
        }
        final CustomScrollView myScrollView = (CustomScrollView)dialog.findViewById(R.id.custom_scrollview);
        final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.rate);
        ratingBar.setMax(5);
        ratingBar.setNumStars(5);
        final GestureOverlayView gestureOverlayView =(GestureOverlayView)dialog.findViewById(R.id.signaturePad);

        TextView tvClear =(TextView)dialog.findViewById(R.id.tv_clear);
        Button btnOk =(Button)dialog.findViewById(R.id.btn_ok);
        Button btnCancel =(Button)dialog.findViewById(R.id.btn_cancel);
        gestureOverlayView.addOnGestureListener(new GestureOverlayView.OnGestureListener() {
            @Override
            public void onGestureStarted(GestureOverlayView overlay, MotionEvent event) {
                myScrollView.setEnableScrolling(false);
            }

            @Override
            public void onGesture(GestureOverlayView overlay, MotionEvent event) {

            }

            @Override
            public void onGestureEnded(GestureOverlayView overlay, MotionEvent event) {
                myScrollView.setEnableScrolling(true);
            }

            @Override
            public void onGestureCancelled(GestureOverlayView overlay, MotionEvent event) {

            }
        });
        tvClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gestureOverlayView.cancelClearAnimation();
                gestureOverlayView.clear(true);
                gestureOverlayView.setDrawingCacheEnabled(true);
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    gestureOverlayView.setDrawingCacheEnabled(true);
                    Bitmap bm = Bitmap.createBitmap(gestureOverlayView.getDrawingCache());
                    File f = new File(Environment.getExternalStorageDirectory() + File.separator + "signatureout.png");
                    f.createNewFile();
                    FileOutputStream os = new FileOutputStream(f);
                    os = new FileOutputStream(f);
                    //compress to specified format (PNG), quality - which is ignored for PNG, and out stream
                    bm.compress(Bitmap.CompressFormat.PNG, 100, os);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    if (bm != null) {
                        imageBase64CheckOut = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    } else {
                        imageBase64CheckOut = "";
                    }
                    os.close();
                    boolean checkOutInput = checkInput(imageBase64CheckOut,etManagerName.getText().toString().trim());
                    if(checkOutInput){
                        dialog.dismiss();
                        prefManager.putString("manager",etManagerName.getText().toString().trim());
                        strCheckOutManagerName =etManagerName.getText().toString().trim();
                        float[] i = new float[]{ratingBar.getRating()};
                        strRating = "" + i[0];
                        if(ConnectivityReceiver.isConnected(MainActivity.this)){
                            new CheckOutFirstTask().execute();
                        } else {
                            Toast.makeText(MainActivity.this, "Internet is not available !", Toast.LENGTH_SHORT).show();
                        }
                    }

                } catch (Exception e) {
                    Log.v("Gestures", e.getMessage());
                    e.printStackTrace();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private void showCheckInDialog() {
        // custom dialog
        final Dialog dialog = new Dialog(context,R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_checkin);
        final EditText etManagerName =(EditText)dialog.findViewById(R.id.et_manager_name);
        if(!TextUtils.isEmpty(prefManager.getString("manager"))){
            etManagerName.setText(prefManager.getString("manager"));
        }
        TextView tvTime = (TextView)dialog.findViewById(R.id.tv_time);
       final CustomScrollView myScrollView = (CustomScrollView)dialog.findViewById(R.id.custom_scrollview);
        final GestureOverlayView gestureOverlayView =(GestureOverlayView)dialog.findViewById(R.id.signaturePad);
        TextView tvClear =(TextView)dialog.findViewById(R.id.tv_clear);
        Button btnOk =(Button)dialog.findViewById(R.id.btn_ok);
        Button btnCancel =(Button)dialog.findViewById(R.id.btn_cancel);
        tvTime.setText("Time of Check In: "+time+"\n"+date);
        gestureOverlayView.addOnGestureListener(new GestureOverlayView.OnGestureListener() {
            @Override
            public void onGestureStarted(GestureOverlayView overlay, MotionEvent event) {
                myScrollView.setEnableScrolling(false);
            }

            @Override
            public void onGesture(GestureOverlayView overlay, MotionEvent event) {

            }

            @Override
            public void onGestureEnded(GestureOverlayView overlay, MotionEvent event) {
                myScrollView.setEnableScrolling(true);
            }

            @Override
            public void onGestureCancelled(GestureOverlayView overlay, MotionEvent event) {

            }
        });
        tvClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gestureOverlayView.cancelClearAnimation();
                gestureOverlayView.clear(true);
                gestureOverlayView.setDrawingCacheEnabled(true);
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                      gestureOverlayView.setDrawingCacheEnabled(true);
                        Bitmap bm = Bitmap.createBitmap(gestureOverlayView.getDrawingCache());
                        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "signatureCheckIn.png");
                        f.createNewFile();
                        FileOutputStream os = new FileOutputStream(f);
                        os = new FileOutputStream(f);
                        //compress to specified format (PNG), quality - which is ignored for PNG, and out stream
                        bm.compress(Bitmap.CompressFormat.PNG, 100, os);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    if (bm != null) {
                        imageBase64CheckIn = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    } else {
                        imageBase64CheckIn = "";
                    }
                         os.close();
                   boolean checkInInPut = checkInput(imageBase64CheckIn,etManagerName.getText().toString().trim());
                    if(checkInInPut){
                        prefManager.putString("manager",etManagerName.getText().toString().trim());
                        dialog.dismiss();
//                        if(ConnectivityReceiver.isConnected(MainActivity.this)){
//                            new ClockInManagerTask().execute();
//                        } else {
//                            Toast.makeText(MainActivity.this, "Internet is not available !", Toast.LENGTH_SHORT).show();
//                        }
                    }
                   } catch (Exception e) {
                        Log.v("Gestures", e.getMessage());
                       e.printStackTrace();
                    }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private boolean checkInput(String base64,String name) {

        if(TextUtils.isEmpty(base64)){
            Toast.makeText(MainActivity.this,"Please take signature",Toast.LENGTH_SHORT).show();
            return false;
        }

        if(TextUtils.isEmpty(name)){
            Toast.makeText(MainActivity.this,"Please write manager name",Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    /**
     * Listener for response to user permission request
     *
     * @param requestCode  Check that permission request code matches
     * @param permissions  Permissions that requested
     * @param grantResults Whether permissions granted
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
               openCameraActivity();
            }

        }else if(requestCode ==2){
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showCheckInDialog();
            }

        }
    }


    /** Async Task for Get CheckOut Task
     *
     * @author akash
     */
    private class ClockInManagerTask extends AsyncTask<String, String, String> {

        private String sb = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            String responseResult = "";

//            String url ="http://70.166.150.196/safeguard/sgSitetracker?CIDate="+date+"&CIOTime="+apitime+"&Command=CheckOut&ErrorNextPage=9999&GuardID="+prefManager.getString("guardId")
//                    +"&InvalidNextPage=1700&ivrJobID="+prefManager.getString("ivrId")+"&JobID=&JobInfoCI=F0000-0000;F0000000000;1&PhoneNumber="+prefManager.getString("phone")
//                    +"&subscriberID=400000&ValidNextPage=1600&Manager="+prefManager.getString("manager");


            String url ="http://104.236.40.68/guardconnect/sg_connect/clockin_validate.php?CIDate="+date+"&CIOTime="+apitime+"&Command=CheckOut&ErrorNextPage=9999"
                    +"&InvalidNextPage=1700&ivrJobID="+prefManager.getString("ivrId")+"&JobID=&JobInfoCI=F0000-0000;F0000000000;1&PhoneNumber="+prefManager.getString("phone")
                    +"&subscriberID=400000&ValidNextPage=1600&Manager="+prefManager.getString("manager");


            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(url);
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                String xml = EntityUtils.toString(httpEntity);
                Log.d("Get checkout Response: ", xml);
                responseResult = xml;

            } catch (Exception e) {
                Log.d("Get checkout Response: ", e.getMessage());
                e.printStackTrace();
            }

            return responseResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("response",result);
            progressDialog.dismiss();
            showPopup();
        }

    }



    /** Async Task for Get CheckOut Task
     *
     * @author akash
     */
    private class CheckOutTask extends AsyncTask<String, String, String> {

        private String sb = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            String responseResult = "";

//            String url ="http://70.166.150.196/safeguard/sgSitetracker?CIDate="+date+"&CIOTime="+apitime+"&Command=CheckOut&ErrorNextPage=9999&GuardID="+prefManager.getString("guardId")
//                    +"&InvalidNextPage=1700&ivrJobID="+prefManager.getString("ivrId")+"&JobID=&JobInfoCI=F0000-0000;F0000000000;1&PhoneNumber="+prefManager.getString("phone")
//                    +"&subscriberID=400000&ValidNextPage=1600&Manager="+prefManager.getString("manager");

            String url ="http://104.236.40.68/guardconnect/sg_connect/sitetracker.php?CIDate="+date+"&CIOTime="+apitime+"&Command=CheckOut&ErrorNextPage=9999&GuardID="+prefManager.getString("guardId")
                    +"&InvalidNextPage=1700&ivrJobID="+prefManager.getString("ivrId")+"&JobID=&JobInfoCI=F0000-0000;F0000000000;1&PhoneNumber="+prefManager.getString("phone")
                    +"&subscriberID=400000&ValidNextPage=1600&Manager="+prefManager.getString("manager");


            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(url);
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                String xml = EntityUtils.toString(httpEntity);
                Log.d("Get checkout Response: ", xml);
                responseResult = xml;

            } catch (Exception e) {
                Log.d("Get checkout Response: ", e.getMessage());
                e.printStackTrace();
            }

            return responseResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("response",result);
            progressDialog.dismiss();
            showPopup();
        }

    }


    /** Async Task for Post First CheckOut Task
    *
            * @author akash
    */
    private class CheckOutFirstTask extends AsyncTask<String, String, String> {

        private String sb = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!progressDialog.isShowing()) {
                progressDialog.setMessage("Please wait...");
                progressDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String responseResult = "";

            String url ="http://70.166.150.196/safeguard/sgSitetracker?CIDate="+date+"&CIOTime="+apitime+"&Command=CheckOut&ErrorNextPage=9999&GuardID="+prefManager.getString("guardId")
                    +"&InvalidNextPage=1700&ivrJobID="+prefManager.getString("ivrId")+"&JobID=&JobInfoCI=F0000-0000;F0000000000;1&PhoneNumber="+prefManager.getString("phone")
                    +"&subscriberID=400000&ValidNextPage=1600&Manager="+strCheckOutManagerName+"&GuardName="+prefManager.getString("name")+"Rating="+strRating+"&SignatureOut="+imageBase64CheckOut
                    +"&SignatureIn="+imageBase64CheckIn;

//            String url ="http://104.236.40.68/guardconnect/sg_connect/sitetracker.php?CIDate="+date+"&CIOTime="+apitime+"&Command=CheckOut&ErrorNextPage=9999&GuardID="+prefManager.getString("guardId")
//                    +"&InvalidNextPage=1700&ivrJobID="+prefManager.getString("ivrId")+"&JobID=&JobInfoCI=F0000-0000;F0000000000;1&PhoneNumber="+prefManager.getString("phone")
//                    +"&subscriberID=400000&ValidNextPage=1600&Manager="+strCheckOutManagerName+"&GuardName="+prefManager.getString("name")+"Rating="+strRating+"&SignatureOut="+imageBase64CheckOut
//                    +"&SignatureIn="+imageBase64CheckIn;


            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httPost = new HttpPost(url);
                HttpResponse httpResponse = httpClient.execute(httPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                String xml = EntityUtils.toString(httpEntity);
                Log.d("Response checkout: ", xml);
                responseResult = xml;

            } catch (Exception e) {
                Log.d("post 1 checkout exce: ", e.getMessage());
                e.printStackTrace();
            }

            return responseResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("response first Checkout",result);
            if(ConnectivityReceiver.isConnected(MainActivity.this)) {
                new CheckOutSecondTask().execute();
            }

        }

    }

    /** Async Task for Post Second CheckOut Task
     *
     * @author akash
     */
    private class CheckOutSecondTask extends AsyncTask<String, String, String> {

        private String sb = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            String responseResult = "";

            String url ="http://104.236.40.68/guardconnect/clockout.php?CIDate="+date+"&CIOTime="+apitime+"&Command=CheckOut&ErrorNextPage=9999&GuardID="+prefManager.getString("guardId")
                    +"&InvalidNextPage=1700&ivrJobID="+prefManager.getString("ivrId")+"&JobID=&JobInfoCI=F0000-0000;F0000000000;1&PhoneNumber="+prefManager.getString("phone")
                    +"&subscriberID=400000&ValidNextPage=1600&Manager="+strCheckOutManagerName+"&GuardName="+prefManager.getString("name")+"Rating="+strRating+"&SignatureOut="+imageBase64CheckOut
                    +"&SignatureIn="+imageBase64CheckIn;


            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httPost = new HttpPost(url);
                HttpResponse httpResponse = httpClient.execute(httPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                String xml = EntityUtils.toString(httpEntity);
                Log.d("Response: ", xml);
                responseResult = xml;

            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("response",result);
            progressDialog.dismiss();
           Intent intent = new Intent(MainActivity.this,FirstScreenActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

    }

}
