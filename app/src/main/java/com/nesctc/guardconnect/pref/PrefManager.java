package com.nesctc.guardconnect.pref;

import android.content.Context;
import android.content.SharedPreferences;


public class PrefManager {

    private static SharedPreferences sharedPreferences;
    private static final String PREF_NAME = "NESCTC";
    private static PrefManager prefManager;
    static Context context;

    private PrefManager() {

    }

    public static PrefManager getInstance(Context context) {
        if (prefManager == null) {
            prefManager = new PrefManager();
            sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
            PrefManager.context = context;
        }

        return prefManager;
    }

    public String getString(String key)
    {
        return sharedPreferences.getString(key,"");
    }

    public void putString(String key,String value)
    {
        SharedPreferences.Editor shEditor = sharedPreferences.edit();
        shEditor.putString(key,value);
        shEditor.commit();
        //Aliss nestct bothpass-12345678
    }


    public int getInt(String key)
    {
        return sharedPreferences.getInt(key,-1);
    }

    public void putInt(String key,int value){

        SharedPreferences.Editor shEditor = sharedPreferences.edit();
        shEditor.putInt(key,value);
        shEditor.commit();
    }

}
