package com.nesctc.guardconnect.service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.nesctc.guardconnect.gpsdetctor.SimpleLocation;
import com.nesctc.guardconnect.pref.PrefManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.util.Timer;
import java.util.TimerTask;




public class LocationUploadService extends Service {

    private Double latitude, longitude;
    private SimpleLocation simpleLocation;
    // timer handling
    private Timer mTimer = null;
    private final int TIME_INTERVAL = 20000;
    private PrefManager prefManager;

    public LocationUploadService() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        Log.e("Google", "Service Created");

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        simpleLocation = new SimpleLocation(this);
        simpleLocation.beginUpdates();
        prefManager = PrefManager.getInstance(this);
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                latitude = simpleLocation.getLatitude();
                longitude = simpleLocation.getLongitude();
                if(latitude ==0.0 && longitude ==0.0){
                    simpleLocation.setListener(new SimpleLocation.Listener() {
                        @Override
                        public void onPositionChanged() {
                            latitude = simpleLocation.getLatitude();
                            longitude = simpleLocation.getLongitude();

                        }
                    });
                }

               // new UploadLatLongTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                new UploadLatLongTaskSecond().execute();

            }
        }, 0, TIME_INTERVAL);


        return super.onStartCommand(intent, flags, startId);

    }


//    /**
//     * Async Task for First Url
//     *
//     * @author akash
//     */
//    private class UploadLatLongTask extends AsyncTask<String, String, String> {
//
//        private String sb = null;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            String responseResult = "";
//
//            String url ="http://www.appently.com/projects/liveview/capture.php?&input_id="+prefManager.getString("ivrId")+"&lat="+latitude+"&lon="+longitude;
//            try {
//                DefaultHttpClient httpClient = new DefaultHttpClient();
//                HttpGet httpGet = new HttpGet(url);
//                HttpResponse httpResponse = httpClient.execute(httpGet);
//                HttpEntity httpEntity = httpResponse.getEntity();
//                String xml = EntityUtils.toString(httpEntity);
//                Log.d("Response  service1: ", xml);
//                responseResult = xml;
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            return responseResult;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//            Log.e("service 1 success",result);
//
//        }
//
//    }
//
//    @Override
//    public void onDestroy() {
//        // TODO Auto-generated method stub
//        super.onDestroy();
//        simpleLocation.endUpdates();
//
//    }


    /**
     * Async Task for Second
     *
     * @author akash
     */
    private class UploadLatLongTaskSecond extends AsyncTask<String, String, String> {

        private String sb = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            String responseResult = "";

            String url ="http://104.236.40.68/guardconnect/capture.php?&input_id="+prefManager.getString("ivrId")+"&lat="+latitude+"&lon="+longitude;
            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(url);
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                String xml = EntityUtils.toString(httpEntity);
                Log.d("Response service2: ", xml);
                responseResult = xml;

            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("service 2 success",result);

        }

    }




}